﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace web.ViewModel
{
    public class NotificationVM
    {
        public string Title { get; set; }
        public string Details { get; set; }
        public System.TimeSpan Time { get; set; }
        public string ImgUrl { get; set; }
    }
}
