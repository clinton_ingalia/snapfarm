﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModel
{
    public class SnapResult
    {
        public Info info { get; set; }
        public Health health { get; set; }
        public List<PestVModel> pestList { get; set; }
        public List<DiseaseVModel> diseaseList { get; set; }
        public List<Expert> expertList { get; set; }
        public List<Store> storeList { get; set; }
        public List<Forum> forumList { get; set; }
        
    }

    public class Info
    {
        public Guid plantId { get; set; }
        public Guid leafId { get; set; }
        public string Title { get; set; }
        public string Species { get; set; }
        public string ImgUrl { get; set; }
        public string Location { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int Age { get; set; }

    }
    public class Health
    {
        public int HealthIndex { get; set; }
        public int SizeIndex { get; set; }
    }
    public class PestVModel
    {
        public Guid pestId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public string Control { get; set; }
    }
    public class DiseaseVModel
    {
        public Guid diseaseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public string Control { get; set; }
    }
    public class Expert
    {
        public Guid profileId { get; set; }
        public string Name { get; set; }
        public string Experience { get; set; }
        public string Location { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }

    public class Store
    {
        public Guid storeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }

    public class Forum
    {
        public Guid forumId { get; set; }
        public string Title { get; set; }
    }
}