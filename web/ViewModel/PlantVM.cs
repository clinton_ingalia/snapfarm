﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModel
{
    public class PlantVM
    {
        public System.Guid ProfileId { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int AgeinDays { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public System.Guid LeafId { get; set; }
        public System.Guid PestId { get; set; }
        public System.Guid DiseaseId { get; set; }
        public string Genus { get; set; }
        public string Species { get; set; }
    }
}