﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModel
{
    public class UserVM
    {
        public string Role { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}