﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Helpers;
using web.Models;
using web.ViewModel;

namespace web.Controllers
{
    public class UsersController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();
        private UserRepo repo = new UserRepo();

        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // POST: api/Login
        [HttpPost]
        [Route("api/Login")]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> Login(UserVM uservm)
        {
            int isRegistered = repo.isRegistered(uservm.Email);
            if (isRegistered == 1)
            {
                var user = repo.Login(uservm);
                if (user != null)
                {
                    return Ok(user);
                }
                else
                {
                    return Content(HttpStatusCode.NotFound, "Email or Password is incorrect");
                }
            }          
            return Content(HttpStatusCode.NotFound, "User not registered");
        }

        // POST: api/changePassword
        [HttpPost]
        [Route("api/changePassword")]
        [ResponseType(typeof(User))]
        public User changePassword(Guid UserId, string newPassword)
        {
            return repo.changePassword(UserId, newPassword);
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(Guid id, UserVM userVM)
        {
            var password = EncryptionHelper.Encrypt(userVM.Password, userVM.Email);
            User user = new User()
            {
                Id = Guid.NewGuid(),
                Email = userVM.Email,
                Password = password,
                Role = userVM.Role,
                Timestamp = DateTime.Now
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(UserVM userVM)
        {

            var password = EncryptionHelper.Encrypt(userVM.Password, userVM.Email);
            User user = new User()
            {
                Id = Guid.NewGuid(),
                Email = userVM.Email,
                Password = password,
                Role = userVM.Role,
                Timestamp = DateTime.Now
            };

            int isRegistered = repo.isRegistered(userVM.Email);

            if (isRegistered == 0)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.Users.Add(user);

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (UserExists(user.Id))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            else
            {
                return Conflict();
            }

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(Guid id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(Guid id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}