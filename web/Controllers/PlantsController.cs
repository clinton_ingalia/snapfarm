﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;
using web.ViewModel;

namespace web.Controllers
{
    public class PlantsController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/Plants
        public IQueryable<Plant> GetPlants()
        {
            return db.Plants;
        }

        // GET: api/Plants/5
        [ResponseType(typeof(Plant))]
        public async Task<IHttpActionResult> GetPlant(Guid id)
        {
            Plant Plant = await db.Plants.FindAsync(id);
            if (Plant == null)
            {
                return NotFound();
            }

            return Ok(Plant);
        }

        // PUT: api/Plants/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPlant(Guid id, Plant Plant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Plant.Id)
            {
                return BadRequest();
            }

            db.Entry(Plant).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Plants
        [ResponseType(typeof(Plant))]
        public async Task<IHttpActionResult> PostPlant(PlantVM PlantVM)
        {
            Plant Plant = new Plant()
            {
                Id = Guid.NewGuid(),
                AgeinDays = PlantVM.AgeinDays,
                Genus = PlantVM.Genus,
                ImgUrl = PlantVM.ImgUrl,
                Latitude = PlantVM.Latitude,
                Longitude = PlantVM.Longitude,
                
            };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Plants.Add(Plant);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PlantExists(Plant.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = Plant.Id }, Plant);
        }

        // DELETE: api/Plants/5
        [ResponseType(typeof(Plant))]
        public async Task<IHttpActionResult> DeletePlant(Guid id)
        {
            Plant Plant = await db.Plants.FindAsync(id);
            if (Plant == null)
            {
                return NotFound();
            }

            db.Plants.Remove(Plant);
            await db.SaveChangesAsync();

            return Ok(Plant);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlantExists(Guid id)
        {
            return db.Plants.Count(e => e.Id == id) > 0;
        }
    }
}