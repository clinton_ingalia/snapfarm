﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;

namespace web.Controllers
{
    public class LeafsController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/Leafs
        public IQueryable<Leaf> GetLeafs()
        {
            return db.Leafs;
        }

        // GET: api/Leafs/5
        [ResponseType(typeof(Leaf))]
        public async Task<IHttpActionResult> GetLeaf(Guid id)
        {
            Leaf Leaf = await db.Leafs.FindAsync(id);
            if (Leaf == null)
            {
                return NotFound();
            }

            return Ok(Leaf);
        }

        // PUT: api/Leafs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLeaf(Guid id, Leaf Leaf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Leaf.Id)
            {
                return BadRequest();
            }

            db.Entry(Leaf).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeafExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Leafs
        [ResponseType(typeof(Leaf))]
        public async Task<IHttpActionResult> PostLeaf(Leaf Leaf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Leafs.Add(Leaf);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LeafExists(Leaf.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = Leaf.Id }, Leaf);
        }

        // DELETE: api/Leafs/5
        [ResponseType(typeof(Leaf))]
        public async Task<IHttpActionResult> DeleteLeaf(Guid id)
        {
            Leaf Leaf = await db.Leafs.FindAsync(id);
            if (Leaf == null)
            {
                return NotFound();
            }

            db.Leafs.Remove(Leaf);
            await db.SaveChangesAsync();

            return Ok(Leaf);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeafExists(Guid id)
        {
            return db.Leafs.Count(e => e.Id == id) > 0;
        }
    }
}