﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;

namespace web.Controllers
{
    public class PaymentMethodsController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/PaymentMethods
        public IQueryable<PaymentMethod> GetPaymentMethods()
        {
            return db.PaymentMethods;
        }

        // GET: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> GetPaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = await db.PaymentMethods.FindAsync(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            return Ok(paymentMethod);
        }

        // PUT: api/PaymentMethods/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPaymentMethod(Guid id, PaymentMethod paymentMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != paymentMethod.Id)
            {
                return BadRequest();
            }

            db.Entry(paymentMethod).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentMethodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PaymentMethods
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> PostPaymentMethod(PaymentMethod paymentMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PaymentMethods.Add(paymentMethod);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PaymentMethodExists(paymentMethod.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = paymentMethod.Id }, paymentMethod);
        }

        // DELETE: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public async Task<IHttpActionResult> DeletePaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = await db.PaymentMethods.FindAsync(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            db.PaymentMethods.Remove(paymentMethod);
            await db.SaveChangesAsync();

            return Ok(paymentMethod);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaymentMethodExists(Guid id)
        {
            return db.PaymentMethods.Count(e => e.Id == id) > 0;
        }
    }
}