﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;

namespace web.Controllers
{
    public class PestsController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/Pests
        public IQueryable<Pest> GetPests()
        {
            return db.Pests;
        }

        // GET: api/Pests/5
        [ResponseType(typeof(Pest))]
        public async Task<IHttpActionResult> GetPest(Guid id)
        {
            Pest Pest = await db.Pests.FindAsync(id);
            if (Pest == null)
            {
                return NotFound();
            }

            return Ok(Pest);
        }

        // PUT: api/Pests/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPest(Guid id, Pest Pest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Pest.Id)
            {
                return BadRequest();
            }

            db.Entry(Pest).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pests
        [ResponseType(typeof(Pest))]
        public async Task<IHttpActionResult> PostPest(Pest Pest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pests.Add(Pest);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PestExists(Pest.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = Pest.Id }, Pest);
        }

        // DELETE: api/Pests/5
        [ResponseType(typeof(Pest))]
        public async Task<IHttpActionResult> DeletePest(Guid id)
        {
            Pest Pest = await db.Pests.FindAsync(id);
            if (Pest == null)
            {
                return NotFound();
            }

            db.Pests.Remove(Pest);
            await db.SaveChangesAsync();

            return Ok(Pest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PestExists(Guid id)
        {
            return db.Pests.Count(e => e.Id == id) > 0;
        }
    }
}