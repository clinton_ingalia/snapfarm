﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using web.Models;
using web.ViewModel;

namespace web.Controllers
{
    public class SnapController : ApiController
    {
        private SnapRepo repo = new SnapRepo();

        // GET: api/Snap
        [HttpGet]
        [Route("api/Snap")]
        public async Task<HttpResponseMessage> Snap(string ImgUrl)
        {
            Info _info = new Info()
            {
                Age = 5,
                ImgUrl = "",
                Latitude = 14,
                Longitude = 32,
                Location = "Juja",
                Species = "Sample",
                Title = "Sample",
                leafId = Guid.NewGuid(),
                plantId = Guid.NewGuid()

            };
            Health _health = new Health()
            {
                HealthIndex = 4,
                SizeIndex = 5
            };
            PestVModel _pest = new PestVModel()
            {  
                pestId = Guid.NewGuid(),
                Control = "Sample Control",
                Description = "Sample Description",
                ImgUrl = "",
                Title = "Sample"
            };
            PestVModel _pest2 = new PestVModel()
            {
                pestId = Guid.NewGuid(),
                Control = "Sample Control",
                Description = "Sample Description",
                ImgUrl = "",
                Title = "Sample 2"
            };
            List<PestVModel> _pestList = new List<PestVModel>();
            _pestList.Add(_pest);
            _pestList.Add(_pest2);
            DiseaseVModel _disease = new DiseaseVModel()
            {
                diseaseId = Guid.NewGuid(),
                Control = "Sample Control",
                Description = "Sample Description",
                ImgUrl = "",
                Title = "Sample"
            };
            DiseaseVModel _disease2 = new DiseaseVModel()
            {
                diseaseId = Guid.NewGuid(),
                Control = "Sample Control",
                Description = "Sample Description",
                ImgUrl = "",
                Title = "Sample 2"
            };
            List<DiseaseVModel> _diseaseList = new List<DiseaseVModel>();
            _diseaseList.Add(_disease);
            _diseaseList.Add(_disease2);
            Expert _expert = new Expert()
            {
                profileId = Guid.NewGuid(),
                Experience = "Sample Experience",
                Latitude = 14,
                Location = "Juja",
                Longitude = 32,
                Name = "Sample"

            };
            Expert _expert2 = new Expert()
            {
                profileId = Guid.NewGuid(),
                Experience = "Sample Experience",
                Latitude = 14,
                Location = "Juja",
                Longitude = 32,
                Name = "Sample 2"

            };
            List<Expert> _expertList = new List<Expert>();
            _expertList.Add(_expert);
            _expertList.Add(_expert2);
            Store _store = new Store()
            {
                storeId = Guid.NewGuid(),
                Description = "Sample Description",
                Latitude = 14,
                Location = "Juja",
                Longitude = 32,
                Name = "Sample"

            };
            Store _store2 = new Store()
            {
                storeId = Guid.NewGuid(),
                Description = "Sample Description",
                Latitude = 14,
                Location = "Juja",
                Longitude = 32,
                Name = "Sample 2"

            };
            List<Store> _storeList = new List<Store>();
            _storeList.Add(_store);
            _storeList.Add(_store2);
            Forum _forum = new Forum()
            {
               forumId = Guid.NewGuid(),
               Title = "Sample"

            };
            Forum _forum2 = new Forum()
            {
                forumId = Guid.NewGuid(),
                Title = "Sample 2"

            };
            List<Forum> _forumList = new List<Forum>();
            _forumList.Add(_forum);
            SnapResult snapResult = new SnapResult()
            {
                info = _info,
                health = _health,
                pestList = _pestList,
                diseaseList = _diseaseList,
                expertList = _expertList,
                storeList = _storeList
            };
            return Request.CreateResponse(HttpStatusCode.OK, snapResult);
        }

       // POST: api/Upload
       //[HttpPost]
       //[Route("api/Upload")]
       //public async Task<HttpResponseMessage> Upload(HttpPostedFileBase file, Guid UserId)
       // {
       //     //if (file == null)
       //     //{
       //     //    return Request.CreateResponse(HttpStatusCode.BadRequest, "No image uploaded");
       //     //}

       //     //if (UserId == null)
       //     //{
       //     //    return Request.CreateResponse(HttpStatusCode.BadRequest, "Null userid");
       //     //}

       //     //var blob = StorageHelper.GetUploadBlobReference(UserId);
       //     //blob.UploadFromStream(file.InputStream);

       //     //StorageHelper.AddImageAddedMessageToQueue(UserId);

       //     //return Request.CreateResponse(HttpStatusCode.OK, UserId);

       // }
    }
}
