﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;

namespace web.Controllers
{
    public class SnapBoardsController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/SnapBoards
        public IQueryable<SnapBoard> GetSnapBoards()
        {
            return db.SnapBoards;
        }

        // GET: api/SnapBoards/5
        [ResponseType(typeof(SnapBoard))]
        public async Task<IHttpActionResult> GetSnapBoard(Guid id)
        {
            SnapBoard snapBoard = await db.SnapBoards.FindAsync(id);
            if (snapBoard == null)
            {
                return NotFound();
            }

            return Ok(snapBoard);
        }

        // PUT: api/SnapBoards/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSnapBoard(Guid id, SnapBoard snapBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != snapBoard.Id)
            {
                return BadRequest();
            }

            db.Entry(snapBoard).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SnapBoardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SnapBoards
        [ResponseType(typeof(SnapBoard))]
        public async Task<IHttpActionResult> PostSnapBoard(SnapBoard snapBoard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SnapBoards.Add(snapBoard);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SnapBoardExists(snapBoard.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = snapBoard.Id }, snapBoard);
        }

        // DELETE: api/SnapBoards/5
        [ResponseType(typeof(SnapBoard))]
        public async Task<IHttpActionResult> DeleteSnapBoard(Guid id)
        {
            SnapBoard snapBoard = await db.SnapBoards.FindAsync(id);
            if (snapBoard == null)
            {
                return NotFound();
            }

            db.SnapBoards.Remove(snapBoard);
            await db.SaveChangesAsync();

            return Ok(snapBoard);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SnapBoardExists(Guid id)
        {
            return db.SnapBoards.Count(e => e.Id == id) > 0;
        }
    }
}