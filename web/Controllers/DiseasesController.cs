﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using web;
using web.Models;

namespace web.Controllers
{
    public class DiseasesController : ApiController
    {
        private snapfarmEntities db = new snapfarmEntities();

        // GET: api/Diseases
        public IQueryable<Disease> GetDiseases()
        {
            return db.Diseases;
        }

        // GET: api/Diseases/5
        [ResponseType(typeof(Disease))]
        public async Task<IHttpActionResult> GetDisease(Guid id)
        {
            Disease Disease = await db.Diseases.FindAsync(id);
            if (Disease == null)
            {
                return NotFound();
            }

            return Ok(Disease);
        }

        // PUT: api/Diseases/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDisease(Guid id, Disease Disease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Disease.Id)
            {
                return BadRequest();
            }

            db.Entry(Disease).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiseaseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Diseases
        [ResponseType(typeof(Disease))]
        public async Task<IHttpActionResult> PostDisease(Disease Disease)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Diseases.Add(Disease);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DiseaseExists(Disease.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = Disease.Id }, Disease);
        }

        // DELETE: api/Diseases/5
        [ResponseType(typeof(Disease))]
        public async Task<IHttpActionResult> DeleteDisease(Guid id)
        {
            Disease Disease = await db.Diseases.FindAsync(id);
            if (Disease == null)
            {
                return NotFound();
            }

            db.Diseases.Remove(Disease);
            await db.SaveChangesAsync();

            return Ok(Disease);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DiseaseExists(Guid id)
        {
            return db.Diseases.Count(e => e.Id == id) > 0;
        }
    }
}