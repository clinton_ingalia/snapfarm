﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using web.ViewModel;

namespace web.Models
{
    interface ISnap
    {
        List<Color> TenMostUsedColours(string ImgUrl);
        List<int> TenMostUsedColourIncidences(string ImgUrl);
        Color MostUsedColour(string ImgUrl);
        int MostUsedColourIncidence(string ImgUrl);
    }
    public class SnapRepo : ISnap
    {
        private snapfarmEntities db = new snapfarmEntities();

        public static List<Color> TenMostUsedColors { get; private set; }
        public static List<int> TenMostUsedColorIncidences { get; private set; }

        public static Color MostUsedColor { get; private set; }
        public static int MostUsedColorIncidence { get; private set; }

        private static int pixelColor;

        private static Dictionary<int, int> dctColorIncidence;

        public Color MostUsedColour(string ImgUrl)
        {
            TenMostUsedColors = new List<Color>();
            TenMostUsedColorIncidences = new List<int>();

            MostUsedColor = Color.Empty;
            MostUsedColorIncidence = 0;

            // does using Dictionary<int,int> here
            // really pay-off compared to using
            // Dictionary<Color, int> ?

            // would using a SortedDictionary be much slower, or ?

            dctColorIncidence = new Dictionary<int, int>();

            //Processing using Bitmap.LockBits and direct memory access in unsafe context with System.Threading.Tasks.Parallel class :

            WebClient client = new WebClient();
            byte[] imageBytes = client.DownloadData(ImgUrl);

            MemoryStream stream = new MemoryStream(imageBytes);
            Bitmap bmp = new Bitmap(stream);

            unsafe
            {

                BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;

                        Console.Write(Color.FromArgb(oldRed, oldBlue, oldGreen).ToString());

                        pixelColor = (Color.FromArgb(oldRed, oldGreen, oldBlue)).ToArgb();

                        for (int col = 0; col < heightInPixels; col++)
                        {
                            if (dctColorIncidence.Keys.Contains(pixelColor))
                            {
                                dctColorIncidence[pixelColor]++;
                            }
                            else
                            {
                                dctColorIncidence.Add(pixelColor, 1);
                            }
                        }




                        //for (int col = 0; col < theBitMap.Size.Height; col++)
                        //{
                        //    pixelColor = theBitMap.GetPixel(row, col).ToArgb();

                        //    if (dctColorIncidence.Keys.Contains(pixelColor))
                        //    {
                        //        dctColorIncidence[pixelColor]++;
                        //    }
                        //    else
                        //    {
                        //        dctColorIncidence.Add(pixelColor, 1);
                        //    }
                        //}
                    }

                    // note that there are those who argue that a
                    // .NET Generic Dictionary is never guaranteed
                    // to be sorted by methods like this
                    var dctSortedByValueHighToLow = dctColorIncidence.OrderByDescending(z => z.Value).ToDictionary(z => z.Key, z => z.Value);

                    // this should be replaced with some elegant Linq ?
                    foreach (KeyValuePair<int, int> kvp in dctSortedByValueHighToLow.Take(10))
                    {
                        TenMostUsedColors.Add(Color.FromArgb(kvp.Key));
                        TenMostUsedColorIncidences.Add(kvp.Value);
                    }

                    MostUsedColor = Color.FromArgb(dctSortedByValueHighToLow.First().Key);
                    MostUsedColorIncidence = dctSortedByValueHighToLow.First().Value;
                });
                bmp.UnlockBits(bitmapData);
                return MostUsedColor;
            }
        }

        public int MostUsedColourIncidence(string ImgUrl)
        {
            TenMostUsedColors = new List<Color>();
            TenMostUsedColorIncidences = new List<int>();

            MostUsedColor = Color.Empty;
            MostUsedColorIncidence = 0;

            // does using Dictionary<int,int> here
            // really pay-off compared to using
            // Dictionary<Color, int> ?

            // would using a SortedDictionary be much slower, or ?

            dctColorIncidence = new Dictionary<int, int>();

            //Processing using Bitmap.LockBits and direct memory access in unsafe context with System.Threading.Tasks.Parallel class :

            WebClient client = new WebClient();
            byte[] imageBytes = client.DownloadData(ImgUrl);

            MemoryStream stream = new MemoryStream(imageBytes);
            Bitmap bmp = new Bitmap(stream);

            unsafe
            {

                BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;

                        Console.Write(Color.FromArgb(oldRed, oldBlue, oldGreen).ToString());

                        pixelColor = (Color.FromArgb(oldRed, oldGreen, oldBlue)).ToArgb();

                        for (int col = 0; col < heightInPixels; col++)
                        {
                            if (dctColorIncidence.Keys.Contains(pixelColor))
                            {
                                dctColorIncidence[pixelColor]++;
                            }
                            else
                            {
                                dctColorIncidence.Add(pixelColor, 1);
                            }
                        }




                        //for (int col = 0; col < theBitMap.Size.Height; col++)
                        //{
                        //    pixelColor = theBitMap.GetPixel(row, col).ToArgb();

                        //    if (dctColorIncidence.Keys.Contains(pixelColor))
                        //    {
                        //        dctColorIncidence[pixelColor]++;
                        //    }
                        //    else
                        //    {
                        //        dctColorIncidence.Add(pixelColor, 1);
                        //    }
                        //}
                    }

                    // note that there are those who argue that a
                    // .NET Generic Dictionary is never guaranteed
                    // to be sorted by methods like this
                    var dctSortedByValueHighToLow = dctColorIncidence.OrderByDescending(z => z.Value).ToDictionary(z => z.Key, z => z.Value);

                    // this should be replaced with some elegant Linq ?
                    foreach (KeyValuePair<int, int> kvp in dctSortedByValueHighToLow.Take(10))
                    {
                        TenMostUsedColors.Add(Color.FromArgb(kvp.Key));
                        TenMostUsedColorIncidences.Add(kvp.Value);
                    }

                    MostUsedColor = Color.FromArgb(dctSortedByValueHighToLow.First().Key);
                    MostUsedColorIncidence = dctSortedByValueHighToLow.First().Value;
                });
                bmp.UnlockBits(bitmapData);
                return MostUsedColorIncidence;
            }
        }

        public List<Color> TenMostUsedColours(string ImgUrl)
        {
            TenMostUsedColors = new List<Color>();
            TenMostUsedColorIncidences = new List<int>();

            MostUsedColor = Color.Empty;
            MostUsedColorIncidence = 0;

            // does using Dictionary<int,int> here
            // really pay-off compared to using
            // Dictionary<Color, int> ?

            // would using a SortedDictionary be much slower, or ?

            dctColorIncidence = new Dictionary<int, int>();

            //Processing using Bitmap.LockBits and direct memory access in unsafe context with System.Threading.Tasks.Parallel class :

            WebClient client = new WebClient();
            byte[] imageBytes = client.DownloadData(ImgUrl);

            MemoryStream stream = new MemoryStream(imageBytes);
            Bitmap bmp = new Bitmap(stream);

            unsafe
            {

                BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;

                        Console.Write(Color.FromArgb(oldRed, oldBlue, oldGreen).ToString());

                        pixelColor = (Color.FromArgb(oldRed, oldGreen, oldBlue)).ToArgb();

                        for (int col = 0; col < heightInPixels; col++)
                        {
                            if (dctColorIncidence.Keys.Contains(pixelColor))
                            {
                                dctColorIncidence[pixelColor]++;
                            }
                            else
                            {
                                dctColorIncidence.Add(pixelColor, 1);
                            }
                        }




                        //for (int col = 0; col < theBitMap.Size.Height; col++)
                        //{
                        //    pixelColor = theBitMap.GetPixel(row, col).ToArgb();

                        //    if (dctColorIncidence.Keys.Contains(pixelColor))
                        //    {
                        //        dctColorIncidence[pixelColor]++;
                        //    }
                        //    else
                        //    {
                        //        dctColorIncidence.Add(pixelColor, 1);
                        //    }
                        //}
                    }

                    // note that there are those who argue that a
                    // .NET Generic Dictionary is never guaranteed
                    // to be sorted by methods like this
                    var dctSortedByValueHighToLow = dctColorIncidence.OrderByDescending(z => z.Value).ToDictionary(z => z.Key, z => z.Value);

                    // this should be replaced with some elegant Linq ?
                    foreach (KeyValuePair<int, int> kvp in dctSortedByValueHighToLow.Take(10))
                    {
                        TenMostUsedColors.Add(Color.FromArgb(kvp.Key));
                        TenMostUsedColorIncidences.Add(kvp.Value);
                    }

                    MostUsedColor = Color.FromArgb(dctSortedByValueHighToLow.First().Key);
                    MostUsedColorIncidence = dctSortedByValueHighToLow.First().Value;
                });
                bmp.UnlockBits(bitmapData);
                return TenMostUsedColors;
            }
        }

        public List<int> TenMostUsedColourIncidences(string ImgUrl)
        {
            TenMostUsedColors = new List<Color>();
            TenMostUsedColorIncidences = new List<int>();

            MostUsedColor = Color.Empty;
            MostUsedColorIncidence = 0;

            // does using Dictionary<int,int> here
            // really pay-off compared to using
            // Dictionary<Color, int> ?

            // would using a SortedDictionary be much slower, or ?

            dctColorIncidence = new Dictionary<int, int>();

            //Processing using Bitmap.LockBits and direct memory access in unsafe context with System.Threading.Tasks.Parallel class :

            WebClient client = new WebClient();
            byte[] imageBytes = client.DownloadData(ImgUrl);

            MemoryStream stream = new MemoryStream(imageBytes);
            Bitmap bmp = new Bitmap(stream);

            unsafe
            {

                BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(bmp.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;

                        Console.Write(Color.FromArgb(oldRed, oldBlue, oldGreen).ToString());

                        pixelColor = (Color.FromArgb(oldRed, oldGreen, oldBlue)).ToArgb();

                        for (int col = 0; col < heightInPixels; col++)
                        {
                            if (dctColorIncidence.Keys.Contains(pixelColor))
                            {
                                dctColorIncidence[pixelColor]++;
                            }
                            else
                            {
                                dctColorIncidence.Add(pixelColor, 1);
                            }
                        }
                            

                        

                        //for (int col = 0; col < theBitMap.Size.Height; col++)
                        //{
                        //    pixelColor = theBitMap.GetPixel(row, col).ToArgb();

                        //    if (dctColorIncidence.Keys.Contains(pixelColor))
                        //    {
                        //        dctColorIncidence[pixelColor]++;
                        //    }
                        //    else
                        //    {
                        //        dctColorIncidence.Add(pixelColor, 1);
                        //    }
                        //}
                    }

                    // note that there are those who argue that a
                    // .NET Generic Dictionary is never guaranteed
                    // to be sorted by methods like this
                    var dctSortedByValueHighToLow = dctColorIncidence.OrderByDescending(z => z.Value).ToDictionary(z => z.Key, z => z.Value);

                    // this should be replaced with some elegant Linq ?
                    foreach (KeyValuePair<int, int> kvp in dctSortedByValueHighToLow.Take(10))
                    {
                        TenMostUsedColors.Add(Color.FromArgb(kvp.Key));
                        TenMostUsedColorIncidences.Add(kvp.Value);
                    }

                    MostUsedColor = Color.FromArgb(dctSortedByValueHighToLow.First().Key);
                    MostUsedColorIncidence = dctSortedByValueHighToLow.First().Value;
                });
                bmp.UnlockBits(bitmapData);
                return TenMostUsedColorIncidences;
            }
        }
    }
}