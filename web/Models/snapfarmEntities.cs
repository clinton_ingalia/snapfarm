﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace web.Models
{
    public class snapfarmEntities : DbContext
    {
        public snapfarmEntities() : base("name=snapfarmEntities")
        {
        }

        public virtual DbSet<Disease> Diseases { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<Leaf> Leafs { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethods { get; set; }
        public virtual DbSet<Pest> Pests { get; set; }
        public virtual DbSet<Plant> Plants { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<SnapBoard> SnapBoards { get; set; }
        public virtual DbSet<User> Users { get; set; }
    }
}