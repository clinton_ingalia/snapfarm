﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Helpers;
using web.ViewModel;

namespace web.Models
{
    interface IUser
    {
        User Login(UserVM user);
        int isRegistered(string email);
        User changePassword(Guid UserId, string newPassword);
        User resetPassword(string email);
    }
    public class UserRepo : IUser
    {
        private snapfarmEntities db = new snapfarmEntities();
        public User Login(UserVM user)
        {
            var nUser = db.Users.Where(oUser => oUser.Email == user.Email && oUser.Role == user.Role).FirstOrDefault();
            var dbPassword = EncryptionHelper.Decrypt(nUser.Password, nUser.Email);
            if (nUser != null && dbPassword == user.Password)
            {
                return nUser;
            }
            return null;
        }

        public int isRegistered(string email)
        {
            var user = db.Users.Where(us => us.Email == email).FirstOrDefault();
            if (user != null)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public User changePassword(Guid userId, string newPassword)
        {
            var oUser = db.Users.Where(u => u.Id == userId).FirstOrDefault();
            var password = EncryptionHelper.Encrypt(newPassword, oUser.Email);
            if (oUser != null)
            {
                oUser.Password = password;
                try
                {
                    db.Entry(oUser).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return oUser;
                }
                catch (Exception r)
                {
                    Console.WriteLine(r.Message);
                    return null;
                }

            }
            return null;
        }

        public User resetPassword(string email)
        {
            //var oUser = db.Users.Where(u => u.Email == email).FirstOrDefault();
            //if (oUser != null)
            //{
            //    oUser.Password = newPassword;
            //    try
            //    {
            //        db.Entry(oUser).State = System.Data.Entity.EntityState.Modified;
            //        db.SaveChanges();
            //        return oUser;
            //    }
            //    catch (Exception r)
            //    {
            //        Console.WriteLine(r.Message);
            //        return null;
            //    }

            //}
            return null;
        }
    }
}