namespace web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using web.Helpers;

    internal sealed class Configuration : DbMigrationsConfiguration<web.Models.snapfarmEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(web.Models.snapfarmEntities context)
        {
            //  This method will be called after migrating to the latest version.

            //Admin User
            var password = EncryptionHelper.Encrypt("1234", "Admin@snapfarm.com");
            context.Users.AddOrUpdate(
                u => u.Id,
                new User { Id = Guid.Parse("7AA6559D-177C-4566-BC0E-E32B290AE010"), Email = "Admin@snapfarm.com", Password = password, Role = "Admin", Timestamp = DateTime.UtcNow }
                );

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
