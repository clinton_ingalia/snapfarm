﻿namespace web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlantId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Prevention = c.String(),
                        Treatment = c.String(),
                        Damage = c.String(),
                        FrontImageUrl = c.String(),
                        BackImageUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plants", t => t.PlantId, cascadeDelete: true)
                .Index(t => t.PlantId);
            
            CreateTable(
                "dbo.Plants",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Name = c.String(),
                        ImgUrl = c.String(),
                        AgeinDays = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Genus = c.String(),
                        Species = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Firstname = c.String(),
                        Lastname = c.String(),
                        Phone = c.String(),
                        ImgUrl = c.String(),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Role = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.Leaves",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlantId = c.Guid(nullable: false),
                        Name = c.String(),
                        FrontImageUrl = c.String(),
                        BackImageUrl = c.String(),
                        Age = c.Int(nullable: false),
                        FrontColor = c.String(),
                        BackColor = c.String(),
                        Spots = c.Boolean(nullable: false),
                        SpotsColor = c.String(),
                        Nitrogen_ = c.Int(nullable: false),
                        Potassium_ = c.Int(nullable: false),
                        Phosphorus_ = c.Int(nullable: false),
                        Calcium_ = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plants", t => t.PlantId, cascadeDelete: true)
                .Index(t => t.PlantId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Title = c.String(),
                        Details = c.String(),
                        ImgUrl = c.String(),
                        ImgUrl2 = c.String(),
                        ImgUrl3 = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Details = c.String(),
                        ImgUrl = c.String(),
                        Time = c.Time(nullable: false, precision: 7),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Partners",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        LogoUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        LogoUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MethodId = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Amount = c.Double(nullable: false),
                        ConfirmationCode = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        PaymentMethod_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethod_Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId)
                .Index(t => t.PaymentMethod_Id);
            
            CreateTable(
                "dbo.Pests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PlantId = c.Guid(nullable: false),
                        Name = c.String(),
                        Genus = c.String(),
                        Species = c.String(),
                        Description = c.String(),
                        Prevention = c.String(),
                        Treatment = c.String(),
                        Damage = c.String(),
                        FrontImageUrl = c.String(),
                        BackImageUrl = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plants", t => t.PlantId, cascadeDelete: true)
                .Index(t => t.PlantId);
            
            CreateTable(
                "dbo.SnapBoards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProfileId = c.Guid(nullable: false),
                        Title = c.String(),
                        Content = c.String(),
                        ImgUrl = c.String(),
                        ImgUrl2 = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Profiles", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.ProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SnapBoards", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Pests", "PlantId", "dbo.Plants");
            DropForeignKey("dbo.Payments", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Payments", "PaymentMethod_Id", "dbo.PaymentMethods");
            DropForeignKey("dbo.PaymentMethods", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.News", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Leaves", "PlantId", "dbo.Plants");
            DropForeignKey("dbo.Feedbacks", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Diseases", "PlantId", "dbo.Plants");
            DropForeignKey("dbo.Plants", "ProfileId", "dbo.Profiles");
            DropForeignKey("dbo.Profiles", "UserId", "dbo.Users");
            DropIndex("dbo.SnapBoards", new[] { "ProfileId" });
            DropIndex("dbo.Pests", new[] { "PlantId" });
            DropIndex("dbo.Payments", new[] { "PaymentMethod_Id" });
            DropIndex("dbo.Payments", new[] { "ProfileId" });
            DropIndex("dbo.PaymentMethods", new[] { "ProfileId" });
            DropIndex("dbo.News", new[] { "ProfileId" });
            DropIndex("dbo.Leaves", new[] { "PlantId" });
            DropIndex("dbo.Feedbacks", new[] { "ProfileId" });
            DropIndex("dbo.Profiles", new[] { "UserId" });
            DropIndex("dbo.Plants", new[] { "ProfileId" });
            DropIndex("dbo.Diseases", new[] { "PlantId" });
            DropTable("dbo.SnapBoards");
            DropTable("dbo.Pests");
            DropTable("dbo.Payments");
            DropTable("dbo.PaymentMethods");
            DropTable("dbo.Partners");
            DropTable("dbo.Notifications");
            DropTable("dbo.News");
            DropTable("dbo.Leaves");
            DropTable("dbo.Feedbacks");
            DropTable("dbo.Users");
            DropTable("dbo.Profiles");
            DropTable("dbo.Plants");
            DropTable("dbo.Diseases");
        }
    }
}
