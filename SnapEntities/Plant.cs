//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace web
{
    using System;
    using System.Collections.Generic;
    
    public partial class Plant
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Plant()
        {
            this.Diseases = new HashSet<Disease>();
            this.Leafs = new HashSet<Leaf>();
            this.Pests = new HashSet<Pest>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid ProfileId { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int AgeinDays { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Genus { get; set; }
        public string Species { get; set; }
        public System.DateTime Timestamp { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Disease> Diseases { internal get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Leaf> Leafs { internal get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pest> Pests { internal get; set; }
        public virtual Profile Profile { internal get; set; }
    }
}
