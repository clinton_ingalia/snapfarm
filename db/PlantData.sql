﻿CREATE TABLE [dbo].[PlantData]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ProfileId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(20) NOT NULL,
	[ImgUrl] VARCHAR(max) NOT NULL, 
    [AgeinDays] INT NOT NULL, 
    [Latitude] FLOAT NOT NULL, 
    [Longitude] FLOAT NOT NULL, 
    [Genus] VARCHAR(50) NOT NULL,
	[Species] VARCHAR(50) NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_PlantData_Profile] FOREIGN KEY ([ProfileId]) REFERENCES [Profile]([Id]), 
)
