﻿CREATE TABLE [dbo].[Notification]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Title] VARCHAR(20) NOT NULL, 
    [Details] VARCHAR(50) NOT NULL, 
	[ImgUrl] VARCHAR(MAX) NOT NULL, 
	[Time] Time NOT NULL, 
    [Timestamp] DATETIME NOT NULL
)
