﻿CREATE TABLE [dbo].[PaymentMethod]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
	[ProfileId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(20) NOT NULL, 
    [Description] VARCHAR(50) NOT NULL, 
    [LogoUrl] VARCHAR(MAX) NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_PaymentMethod_Profile] FOREIGN KEY ([ProfileId]) REFERENCES [Profile]([Id])
)
