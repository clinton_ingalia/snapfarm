﻿CREATE TABLE [dbo].[DiseaseData]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
	[PlantId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(20) NOT NULL,
	[Description] VARCHAR(50) NOT NULL,
	[Prevention] VARCHAR(50) NOT NULL,
	[Treatment] VARCHAR(50) NOT NULL,
	[Damage] VARCHAR(50) NOT NULL,
	[FrontImageUrl] VARCHAR(max) NOT NULL,
	[BackImageUrl] VARCHAR(max) NOT NULL,  
    [Timestamp] DATETIME NOT NULL,
    CONSTRAINT [FK_DiseaseData_PlantData] FOREIGN KEY ([PlantId]) REFERENCES [PlantData]([Id])
)
