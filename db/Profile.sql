﻿CREATE TABLE [dbo].[Profile]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [UserId] UNIQUEIDENTIFIER NOT NULL, 
    [Firstname] VARCHAR(50) NOT NULL, 
    [Lastname] VARCHAR(50) NOT NULL, 
    [Phone] VARCHAR(50) NOT NULL, 
    [ImgUrl] VARCHAR(MAX) NOT NULL, 
    [Latitude] FLOAT NOT NULL, 
    [Longitude] FLOAT NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_Profile_User] FOREIGN KEY ([UserId]) REFERENCES [User]([Id])
)
