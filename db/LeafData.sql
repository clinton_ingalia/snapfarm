﻿CREATE TABLE [dbo].[LeafData]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
	[PlantId] UNIQUEIDENTIFIER NOT NULL,
    [Name] VARCHAR(20) NOT NULL,
	[FrontImageUrl] VARCHAR(max) NOT NULL,
	[BackImageUrl] VARCHAR(max) NOT NULL, 
    [Age] INT NOT NULL, 
    [FrontColor] VARCHAR(20) NOT NULL, 
    [BackColor] VARCHAR(20) NOT NULL, 
    [Spots] BIT NOT NULL, 
    [SpotsColor] NCHAR(10) NOT NULL, 
    [Nitrogen%] INT NOT NULL, 
    [Potassium%] INT NOT NULL, 
    [Phosphorus%] INT NOT NULL, 
    [Calcium%] INT NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_LeafData_PlantData] FOREIGN KEY ([PlantId]) REFERENCES [PlantData]([Id])
)
