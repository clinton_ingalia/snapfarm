﻿CREATE TABLE [dbo].[Feedback]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ProfileId] UNIQUEIDENTIFIER NOT NULL, 
    [Title] NCHAR(20) NOT NULL, 
    [Description] VARCHAR(50) NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_Feedback_Profile] FOREIGN KEY ([ProfileId]) REFERENCES [Profile]([Id])
)
