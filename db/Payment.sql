﻿CREATE TABLE [dbo].[Payment]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [MethodId] UNIQUEIDENTIFIER NOT NULL, 
    [ProfileId] UNIQUEIDENTIFIER NOT NULL, 
    [Amount] FLOAT NOT NULL, 
    [ConfirmationCode] VARCHAR(50) NOT NULL, 
    [Timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_Payment_PaymentMethod] FOREIGN KEY ([MethodId]) REFERENCES [PaymentMethod]([Id]),
	CONSTRAINT [FK_Payment_Profile] FOREIGN KEY ([ProfileId]) REFERENCES [Profile]([Id])
)
